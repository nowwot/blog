---
layout: post
title: "Then and now for Nowwot: A moment of boredom to a fully fledged startup"
---

Hi everyone,

Welcome to the Nowwot blog! Here at Nowwot, we wanted to document our journey, a raw picture of our fortunes and tribulations as we embark on our journey through the [Lightning Lab business accelerator](http://www.lightninglab.co.nz/lightning-lab-christchurch-announces-10-startups-in-2015-programme/).

It all started in December last year when Max was with his flat in Christchurch, with all his flatmates sitting in the lounge. They were all bored and just browsing the net on their phones. To break the norm, they took a jar and got all the flatmates to write a group activity they would like to do on a piece of paper, and drop it into the jar. Once they had done this, they would draw an activity, then out of spontaneity, go out actually do the activity. This resulted in a fun and exciting way to break the norm for Max and his flat.

They continued to do this whenever the flat were in a bit of a rut of flat boredom. Unfortunately, after building the number of activities in the jar, they soon realised that most the time, the activity was unsuitable to current context they were in, such drawing the “Go to the West Coast for the weekend” wouldn’t work on a Tuesday. Realising this issue, Max looked at making an app for this, and recognising both the potential and complexity of a platform that could solve this problem, he looked to a good friend that he both worked with and is familiar with building software platforms.

Max approached Charlie with the idea, who had just recently moved to Wellington to take software development for a business internet company. This happens to Charlie often, as with most “app” developers. But instead of shutting Max down, he saw potential in the idea, and agreed to pursue it after-hours, leading the development of the platform.

Months go by and we have a basic working prototype demonstrating our core feature. Sick of spending spare time working on the idea, Charlie throws the idea of entering Lightning Lab. For those that don’t know what Lightning Lab (LL) is:

> Lightning Lab’s 12-week mentor-intensive programme is based on the best of breed
accelerator model developed by [TechStars](http://www.techstars.com/) in the US.

> Lightning Lab is the only New Zealand member of the Global Accelerator Network (GAN), an invite-only community of the world’s most respected organizations that provide startups with the best resources to create and grow their businesses, wherever they are. Startups accepted into a GAN accelerator have access to a variety of exclusive perks including over $1M worth of free services, introductions to a network of investors and access to soft landing spaces worldwide

Entry into the lab would mean connections to those who would be able help accelerate their startup to stardom, initial seed money of $18,000, and the belief that they are capable to building a viable business from an idea. 

Charlie is already familiar with LL, with his friend Tom Harding been through the Wellington 2014 lab. The commencement date was about right for Max, marking a target end date for his doctorate thesis, and Charlie was pretty flexible, expect it would result in a resignation from his current job.

We were proactive with our application, starting off with Charlie going to the LL Chch 2015 Information evening at CreativeHQ in Wellington and having a conversation with Charlie Tomlinson. We continually refined our application with feedback from the LL Chch team. The application process culminated in 10 minute interview with a selected panel of entrepreneurs, mentors and investors.

The very next evening, we were informed that we have secured a position in the programme! The team was ecstatic. All effort now was put into preparing for the LL programme. Max ramped up his effort on his PhD thesis to try finish his draft by the start of the programme. Unfortunately, Charlie had to bear bad news to his boss the next day about his impending departure. 

Fast forward a frantic month of trying to close our current affairs to focus on the lab, we finally met as a group for the first time since May at C1 cafe, before an PR event Max was to attend. Two days before this meeting, Max and Charlie made a decision to hire a contractor, who had applied to be a lab tech for the LL programme, to provide support to our team, especially legal and financial issues, which wasn’t a strong skill amongst Max or Charlie. Phil, our newest team member, was hired and was present at the meeting as well.

While it didn’t mark the beginning of the LL Programme, the majority of the teams participated in the Pre-Programme last Friday. It was a light-hearted affair, mostly around getting to know each other, administration things and helpful tips from some speakers.

The official start of the programme is next Monday. This is the moment that we’ve been waiting for, to be able to focus on building a platform to help groups solve the common problem of finding and doing activities with each other. Not only are we dedicated now, we will now have the support of the LL programme, that will help us connect with entrepreneurs and mentors who had done the same thing, allowing us to quickly learn in record time. We are looking forward to sharing this journey will you all as well!

Signing out,

Team Nowwot

