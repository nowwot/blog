---
layout: post
title: "Full throttle and the wall: Week 1 in LL"
---
Hi everyone, 

The day has come, the official beginning of the LL programme. It was an exciting atmosphere, not knowing what was going to be ahead of us. The entire lab met at the beginning of the day, for a rundown on how the programme will work, and a small introduction in the form of a pitch between teams. The rest of the day spent sorting through things and shortlisting 20 for an available 70 mentors for helping our team grow into a business producing a commercially viable product.

The next day, we were already hit with 4 mentor meetings. Some of the mentors were dubious of our vision, asking some tough questions that we weren’t so prepared for. Initially it was a little heart-breaking but the feedback, good or bad, was always constructive so at the end of the day, it was an extremely helpful exercise. In between the mentor meetings, we were absorbing the feedback, preparing for market research and ticking off an endless list of business administration.

This pattern continued has continued each day this week. Virtually no product development has happened, as we spend time to work on our lean canvas, a tool used to communicate our business model in a succinct format, allowing us to identify strengths, weaknesses, and areas we haven’t gathered validated data for

With every mentor meeting, each had a different approach to the meeting and each had their own personal opinion about our vision and product. While the overall feedback is that our idea has potential, the sheer number of polarising perspectives of its application has been overwhelming. To the point that at times, we weren’t sure if this really was for.

Ultimately we are the ones to live out the vision for our idea, so we are evaluating each avenue critically.

The best form of evaluation is actually trying to validate our lean canvas, beginning at the problem-customer relationship, by simply talking to our target customers. It has been a humbling experience, conversation after conversation, the realisation that the problem we aimed to solve with our product, is not as strong as we expected.

Each week ends with a good, bad, ugly session which collectively, we get to hear about the ups and downs of each team. Virtually all the teams concurred with the mentor meetings being a draining but necessary process, and despite varying progressions into product development between teams, everyone has taken a step back to square one, and trying to truly understand the customer and problem they are trying to solve.

Overall, it has been a busy week, meeting mentors everyday and discussing our product. Pretty much, doing exactly what the programme had planned for us, which was finding our direction. There is still much ahead for us for narrowing and selecting what direction is the most commercially viable but important, as a team, can be passionate to bringing to fruition.

Signing out,

Team Nowwot
