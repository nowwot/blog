---
layout: page
title: About
---

This blog will provide a personal reflection of Nowwot, specifically, the journey they are embarking on from an idea to their attempt to revolutionize the way groups organise and discover new activities to do with each other. Our aim is to be able to communicate with our loved ones, friends, and the wider world about our startup story, regardless if whether we succeed or not, and hope that anyone reading this will be able to benefit.

With love,

Team Nowwot
